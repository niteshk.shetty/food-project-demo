# FOOD PROJECT

![images/microservices.png](images/microservices.png)

Food project - is a microservice architecture example project developed with Java/Kotlin and SpringBoot. This
application like delivery-club. Many restaurants offer their dishes to customers. Customers can choose their favorite
food and order it. The application has two frontends: one for customers and the other for restaurateurs.

- [Order service](https://gitlab.com/foodprojectdemo/order-service) - a core reactive RESTFul microservice with Kotlin,
  DDD and Event-driven architecture, Spring WebFlux, MongoDB and Apache Kafka.
- [Notification service](https://gitlab.com/foodprojectdemo/notification-service) - so far only a simple email sending
  service with reliable idempotent and retry sending.
- [Restaurant service](https://gitlab.com/foodprojectdemo/restaurant-service) - a secondary microservice developed on
  Java with easier domain logic, a kind of simple storage (CRUD) for restaurant entities.
- [Shopping UI Gateway](https://gitlab.com/foodprojectdemo/shopping-ui-gateway) - a UI Gateway for customers where they
  can order their favorite dishes.
- [Restaurant UI Gateway](https://gitlab.com/foodprojectdemo/restaurant-ui-gateway) - a UI Gateway for restaurateurs and
  their stuff where they can manage dishes and accept tickets for making order(login: brynn.parker@hotmail.com password:
  password).
- [Acceptance testing](https://gitlab.com/foodprojectdemo/acceptance-testing) - end to end integration tests of
  microservices with selenium.

## Running the application

1. First run the following commands:

```
git clone --recurse-submodules https://gitlab.com/foodprojectdemo/food-project-demo.git food-project-demo
cd !$
make docker-run
```

2. Go to [Prometheus](http://localhost:9090/targets) to make sure all services are running.

3. Go to [Shopping UI Gateway](http://localhost:8082/). You can choose a restaurant (for example, Burger King) and order
   a few dishes. Also, you can log in as a demo user (``user``/``password``).

4. Go to [Mail dev](http://localhost:1080/). Here you can check that the notification has been received.

5. After checkout, go to [Restaurant UI Gateway](http://localhost:8084/). Use following
   credentials: ``brynn.parker@hotmail.com``/``password`` for log in as a Burger King restaurateur. There is a new
   ticket for your dishes. You can accept or reject the ticket.

6. In [My Orders](http://localhost:8082/my/orders) you will see that the order status has been changed.

## Locations

- [http://localhost:8082/](http://localhost:8082/) - Shopping UI Gateway
- [http://localhost:8084/](http://localhost:8084/) - Restaurant UI Gateway (``brynn.parker@hotmail.com``/``password``)
- [http://localhost:9411/](http://localhost:9411/) - Zipkin - a distributed tracing system
- [http://localhost:1080/](http://localhost:1080/) - Maildev - checking mail delivery
- [http://localhost:5601/](http://localhost:5601/) - Kibana - a log monitoring web-interface
- [http://localhost:9090/](http://localhost:9090/targets) - Prometheus - monitoring system
- [http://localhost:3000/](http://localhost:3000/) - Grafana Dashboards analytics monitoring solution (``grafana``/``grafana``)

## Technical Overview

The microservices apply the Choreography pattern for interaction.

## Processing orders

The following UML diagram shows the order processing flow:
![images/order_processing.png](images/order_processing.png)

### Bounded context mapping

![images/bounded_context.png](images/bounded_context.png)

## Technologies

Used the following technologies:

- ``Java/Kotlin`` programming languages
- Reactive Programming with ``Project Reactor``
- Consumer Driven Contracts approach with ``Spring Cloud Contract``
- Distributed Tracing with ``Spring Cloud Sleuth`` and ``Spring Cloud Zipkin``
- Monitoring system with ``Prometheus`` and ``Grafana``
- ``Apache Kafka`` as a powerful message broker
- ``MongoDB``
- ``PostgreSQL``
- ``Selenium/Selenium`` Grid for acceptance tests
- ``WireMockServer`` for external stubs